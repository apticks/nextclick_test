<!--Add Sub_Category And its list-->
<div class="row">
    <div class="col-12">
        <h4 class="ven">Add Support</h4>
        <form class="needs-validation" novalidate="" action="<?php echo base_url('support/list');?>" method="post" enctype="multipart/form-data">
            <div class="card-header">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label>Filter</label>
                        <!-- <input type="file" class="form-control" required="">-->
                        <input type="date" name="fromdate" id="frmDt" />
                    </div>
                    <div class="form-group col-md-4">
                        <label>Date 2</label>
                        <!-- <input type="file" class="form-control" required="">-->
                        <input type="date" name="todate" id="toDt"/>
                    </div>
                    <div class="form-group col-md-4">
                        <button class="btn btn-primary mt-27" id="btnFilter">Filter</button>
                    </div>
                </div>
            </div>
        </form>
    </div> 
    <div class="card-body">
        <div class="card">
            <div class="card-header">
                <h4 class="ven">List of Request's</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="tableExport" style="width: 100%;">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Request's</th>
                                <th>Contact Mail</th>
                                <th>Request Content</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($requests)):?>
                            <?php $sno = 1; foreach ($requests as $req):?>
                            <tr>
                                <td><?php echo $sno++;?></td>
                                <td><?php foreach ($request_type as $category):?>
                                        <?php echo ($category['id'] == $req['req_id'])? $category['title']:'';?>
                                <?php endforeach;?></td>
                                <td><?php echo $req['contact_mail'];?></td>
                                <td><?php echo $req['req_content'];?></td>

                                <td>
                                    <a href="<?php echo base_url()?>amenity/edit?id=<?php echo $req['id'];?>" class="mr-2"> <i class="fas fa-pencil-alt"></i> </a>
                                    <a href="#" class="mr-2 text-danger" onClick="delete_record(<?php echo $req['id'] ?>, 'amenity')"> <i class="far fa-trash-alt"></i> </a>
                                </td>
                            </tr>
                            <?php endforeach;?>
                            <?php else :?>
                            <tr>
                                <th colspan="5">
                                    <h3><center>No Request's</center></h3>
                                </th>
                            </tr>
                            <?php endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>
<script type="text/javascript">
//     $(document).ready(function(){
// $('#btnFilter').click(function(){
// alert('clicked');
// });

//     });
</script>
